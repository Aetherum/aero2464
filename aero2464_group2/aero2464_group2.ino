#include <Servo.h>

Servo ball;
Servo syringe;

int ballPin = 5;
int syringePin = 6;
int buzzerPin = 4;
int ledPin = 3;

int readPin = 8;

int frequencies[3];
int counter = 0;
int lastFreq = 0;


boolean fiveTrigger = false;
boolean tenTrigger = false;
boolean twentyTrigger = false;
boolean fourtyTrigger = false;

void setup() {
  // put your setup code here, to run once:
  pinMode(readPin, INPUT);
  pinMode(ledPin, OUTPUT);
  pinMode(buzzerPin, OUTPUT);
  Serial.begin(9600);

  ball.attach(ballPin);
  syringe.attach(syringePin);
  ball.write(90);
  syringe.write(90);

  digitalWrite(ledPin, HIGH);

}

void loop() {
  checkTriggers(readFrequency());
  activateOutputs();

}

void activateOutputs() {
  if(fiveTrigger) {
    //Nothing
    fiveTrigger = false;
    //Turn LED off and on again to identify no action beacon
    digitalWrite(ledPin, LOW);
    delay(5000);
    digitalWrite(ledPin, HIGH);
  }
  if(tenTrigger) {
    //Buzzer
    tenTrigger = false;
    //One second buzzer beep
    digitalWrite(buzzerPin, HIGH);
    delay(1000);
    digitalWrite(buzzerPin, LOW);
  }
  if(twentyTrigger) {
    //Syringe
    twentyTrigger = false;
    //3 Second delay before dropping syringe, while flashing LED to notify
    for(int i = 0; i <= 3000; i+=100) {
      digitalWrite(ledPin, HIGH);
      delay(100);
      digitalWrite(ledPin, LOW);
      delay(100);
      if(i == 3000) {
        digitalWrite(ledPin, HIGH);
        syringe.write(0);
        delay(1500);
        syringe.write(90);   
      }
    }  
  }
  if(fourtyTrigger) {
    //Ball
    fourtyTrigger = false;  
    //3 Second delay before dropping ball, while flashing LED to notify
    for(int i = 0; i <= 3000; i+=100) {
      digitalWrite(ledPin, HIGH);
      delay(100);
      digitalWrite(ledPin, LOW);
      delay(100);
      if(i == 3000) {
        digitalWrite(ledPin, HIGH);
        ball.write(0);
        delay(1500);
        ball.write(90);   
      }
    }   
  }
  
}

void checkTriggers(float frequency) {
  /* Checks whether frequency input is within a specified range representing beacon frequencies
   *  if the previous 2 frequencies read are also inside the range, it is accepted as a valid frequency
   *  Slight offsets are necessary due to a slight inaccuracy of the signal duty cycle
   */
  if(frequency > 4 && frequency < 6) {
      if(lastFreq == 5) {
          if(counter < 3) {
          frequencies[counter] = frequency;
          counter++;
        } else {
          float avgFreq = (frequencies[0] + frequencies[1] + frequencies[2])/3;
          if(avgFreq > 4 && avgFreq < 6) {
            fiveTrigger = true;
            counter = 0;
          }
        }
      } 
      lastFreq = 5;
  } else if(frequency > 8 && frequency < 12) {
      if(lastFreq == 10) {
          if(counter < 3) {
          frequencies[counter] = frequency;
          counter++;
        } else {
          float avgFreq = (frequencies[0] + frequencies[1] + frequencies[2])/3;
          if(avgFreq > 8 && avgFreq < 12) {
            tenTrigger = true;
            counter = 0;
          }
        }
      }
      lastFreq = 10;   
  } else if(frequency > 18 && frequency < 22) {
      if(lastFreq == 20) {
          if(counter < 3) {
          frequencies[counter] = frequency;
          counter++;
        } else {
          float avgFreq = (frequencies[0] + frequencies[1] + frequencies[2])/3;
          if(avgFreq > 18 && avgFreq < 22) {
            twentyTrigger = true;
            counter = 0;
          }
        }
      }
      lastFreq = 20;   
  } else if(frequency > 38 && frequency < 45) {
      if(lastFreq == 40) {
          if(counter < 3) {
          frequencies[counter] = frequency;
          counter++;
        } else {
          float avgFreq = (frequencies[0] + frequencies[1] + frequencies[2])/3;
          if(avgFreq > 38 && avgFreq < 45) {
            fourtyTrigger = true;
            counter = 0;
          }
        }
      }
      lastFreq = 40;   
  } else {
    counter = 0;
    lastFreq = 0; 
  }
}

float readFrequency() {
  //Reads length of pulse provided by pulseIn, and converts to a float representing the frequency of the signal 
  unsigned long duration = pulseIn(readPin, LOW);
  double period = 0.000002*duration;
  double frequency = 1/period;
  return (float) frequency;
}
